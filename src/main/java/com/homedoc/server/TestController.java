package com.homedoc.server;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@GetMapping("/tata")
	public String hello() {
		return "Bonjour Victor et Baptiste";
	}
}
